from app import create_app, db

from app.models.user_role import UserRole
from app.models.permission import Permission
from app.models.role_permission import RolePermission
from app.models.role import Role
from app.models.user import User

app = create_app()

@app.shell_context_processor
def make_shell_context():
    return {
        'db': db,
        'User': User,
        'UserRole': UserRole,
        'Role': Role,
        'Permission': Permission,
        'RolePermission': RolePermission
    }