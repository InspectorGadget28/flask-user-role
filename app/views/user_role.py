from app import db
from app.views import blueprint

from flask import jsonify, abort, request

from app.models.user import User
from app.models.role import Role
from app.models.user_role import UserRole

@blueprint.route('/users/<string:sam_account_name>', methods=['POST'])
def assign_role_to_user(sam_account_name: str):
    if not request.json:
        return abort(400, "Not a JSON")

    if 'role' not in request.json:
        return abort(400, "One or more param not satisfied.")

    role = request.json['role']

    user = User.query.filter_by(sam_account_name=sam_account_name).first()

    if not user:
        return abort(404, "User not found.")

    if not User.has_user(user.id):
        return abort(404, "User not found.")

    if not Role.has_role(role):
        return abort(404, "Role not found.")

    role = Role.query.filter_by(name=role).first()

    if UserRole.has_already_assigned(user.id, role.id):
        return abort(400, "Role already assiged to User.")

    new_user_role = UserRole(
        role_id=role.id,
        user_id=user.id
    )
    db.session.add(new_user_role)
    db.session.commit()

    return jsonify({
        "message": "Role has been successfully assigned."
    }), 201
