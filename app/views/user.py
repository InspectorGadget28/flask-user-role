from app import db
from app.models.role import Role
from app.views import blueprint
from flask import jsonify, request, abort

from app.models.user import User

@blueprint.route('/users', methods=['GET'])
def get_users():
    users = [user.serialize() for user in User.query.all()]
    return jsonify(users), 200

@blueprint.route('/users', methods=['POST'])
def add_user():
    if not request.json:
        return abort(400, "Not a JSON")

    if 'sam_account_name' not in request.json:
        return abort(400, "One or more param not satisfied.")

    sam_account_name = request.json['sam_account_name']
    
    has_user: bool = (
        User
        .query
        .filter(
            db.func.lower(
                User.sam_account_name
            ) == sam_account_name.lower()
        )
        .with_entities(db.func.count())
        .scalar()
    ) > 0

    if has_user:
        return abort(400, "User with the username of " + sam_account_name + " exists!")

    new_user = User(sam_account_name=sam_account_name)
    db.session.add(new_user)
    db.session.commit()

    return jsonify(new_user.serialize()), 201


    
    
