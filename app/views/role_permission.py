from app.models.role_permission import RolePermission
from app.models.role import Role
from app.models.permission import Permission
from app.views import blueprint

from app import db

from flask import request, abort, jsonify

@blueprint.route('/roles/<string:name>', methods=['POST'])
def assign_permission_to_role(name: str):
    if not request.json:
        return abort(400, "Not a JSON")

    if 'identifier' not in request.json:
        return abort(400, "One or more param not satisfied.")

    identifier = request.json['identifier']

    role = Role.query.filter_by(name=name).first()

    if not role:
        return abort(404, "Role not found.")
    
    if not Role.has_role(role.name):
        return abort(400, "Role with name " + name + " does not exist.")

    if not Permission.has_identifier(identifier):
        return abort(400, "Permission with identifier '" + identifier + "' does not exist.")

    if RolePermission.has_already_assigned(identifier, role.id):
        return abort(400, "Permission already assiged to Role.")

    new_role_permission = RolePermission(
        role_id=role.id,
        permission=identifier
    )
    db.session.add(new_role_permission)
    db.session.commit()

    return jsonify({
        "message": "Permission has been successfully assigned."
    }), 201
