from app import db
from app.views import blueprint
from flask import jsonify, request, abort

from app.models.role import Role

@blueprint.route('/roles', methods=['GET'])
def get_roles():
    roles = [role.serialize() for role in Role.query.all()]
    return jsonify(roles), 200


@blueprint.route('/roles', methods=['POST'])
def add_role():
    if not request.json:
        return abort(400, "Not a JSON")
    
    if 'name' not in request.json:
        return abort(400, "One or more param not satisfied.")

    name = request.json['name'].strip()

    if Role.has_role(name):
        return abort(400, "Role with the name exists!")

    new_role = Role(name=name)
    db.session.add(new_role)
    db.session.commit()

    return jsonify(new_role.serialize()), 201


@blueprint.route('/roles/<string:role>', methods=['PUT'])
def update_role(role: str):
    if not request.json:
        return abort(400, "Not a JSON")

    if 'name' not in request.json:
        return abort(400, "One or more param not satisfied.")

    name = request.json['name'].strip()

    if not Role.has_role(role):
        return abort(400, "Role with the name does exists!")

    if Role.has_role(name):
        return abort(400, "You are updating to a existing role name!")

    role = Role.query.filter_by(name=role).first()

    role.name = name
    db.session.add(role)
    db.session.commit()

    return jsonify(role.serialize()), 200

    
