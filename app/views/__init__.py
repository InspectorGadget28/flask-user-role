from flask import Blueprint

blueprint = Blueprint('views', __name__)

from app.views import user
from app.views import role
from app.views import user_role
from app.views import permission
from app.views import role_permission