from app.models import permission
from app.models.permission import Permission
from app.views import blueprint

from app import db

from flask import request, abort, jsonify

@blueprint.route('/permissions', methods=['GET'])
def get_permissions():
    permissions = [permission.serialize() for permission in Permission.query.all()]
    return jsonify(permissions), 200

@blueprint.route('/permissions', methods=['POST'])
def add_permission_node():
    if not request.json:
        return abort(400, "JSON not found")

    if 'description' not in request.json \
        or 'identifier' not in request.json:
        return abort(400, "One or more param not satisfied.")

    description = request.json['description']
    identifier = request.json['identifier']

    if Permission.has_identifier(identifier):
        return abort(400, "Permission with identifier '" + identifier + "' exists.")

    new_permission = Permission(
        identifier=identifier,
        description=description
    )
    db.session.add(new_permission)
    db.session.commit()

    return jsonify(new_permission.serialize()), 201
