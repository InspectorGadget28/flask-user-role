from app import db

class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(20), nullable=False)

    __table_args__ = (
        db.UniqueConstraint('name', name='unique_role_name'),
    )

    role_permissions = db.relationship(
        'Permission',
        secondary="role_permission",
        lazy="dynamic"
    )

    def serialize(self):
        return {
            "name": self.name,
            "permissions": [
                role_permission.serialize()
                for role_permission in self.role_permissions
            ]
        }

    def has_role(role: str) -> bool:
        return (
            Role
            .query
            .filter_by(name=role)
            .with_entities(db.func.count())
            .scalar()
        ) > 0