from app.models import permission
from app import db

class RolePermission(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    role_id = db.Column(
        db.Integer,
        db.ForeignKey("role.id"),
        onupdate="cascade",
        nullable=False
    )
    permission = db.Column(
        db.String(64),
        db.ForeignKey("permission.identifier"),
        nullable=False
    )

    @staticmethod
    def has_already_assigned(identifier: str, role_id: int) -> bool:
        return (
            RolePermission
            .query
            .filter(
                (
                    db.func.lower(
                        RolePermission.permission
                    ) == identifier.lower()
                ) & (
                    RolePermission.role_id == role_id
                )
            )
            .with_entities(db.func.count())
            .scalar()
        ) > 0
