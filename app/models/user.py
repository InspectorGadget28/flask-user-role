from app import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    sam_account_name = db.Column(db.String(20), nullable=False)

    __table_args__ = (
        db.UniqueConstraint('sam_account_name', name='unique_sam_account_name'),
    )

    user_roles = db.relationship(
        'Role',
        secondary="user_role",
        lazy="dynamic"
    )

    def serialize(self):
        return {
            "sam_account_name": self.sam_account_name,
            "roles": [
                user_role.serialize()
                for user_role in self.user_roles
            ]
        }

    def has_user(user_id: int) -> bool:
        return (
            User
            .query
            .filter(
                User.id == user_id
            )
            .with_entities(db.func.count())
            .scalar()
        ) > 0