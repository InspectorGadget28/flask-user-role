from re import M
from app import db

class UserRole(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(
        db.Integer,
        db.ForeignKey("user.id"),
        nullable=False
    )
    role_id = db.Column(
        db.Integer,
        db.ForeignKey("role.id"),
        onupdate="cascade",
        nullable=False
    )

    @staticmethod
    def has_already_assigned(user_id: int, role_id: int) -> bool:
        return (
            UserRole
            .query
            .filter(
                (
                    UserRole.user_id == user_id
                ) & (
                    UserRole.role_id == role_id
                )
            )
            .with_entities(db.func.count())
            .scalar()
        ) > 0