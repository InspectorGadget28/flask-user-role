from app import db

class Permission(db.Model):
    identifier = db.Column(
        db.String(64),
        primary_key=True
    )
    description = db.Column(
        db.String(100),
        nullable=False
    )

    def serialize(self):
        return {
            "identifier": self.identifier,
            "description": self.description
        }

    @staticmethod
    def has_identifier(identifier: str) -> bool:
        return (
            Permission
            .query
            .filter(
                db.func.lower(
                    Permission.identifier
                ) == identifier.lower()
            )
            .with_entities(db.func.count())
            .scalar()
        ) > 0
