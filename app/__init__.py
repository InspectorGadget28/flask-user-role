from config import Config
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
migrate = Migrate()
db.configure_mappers()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)
    db.configure_mappers()

    from app.errors import blueprint as errors_bp
    app.register_blueprint(errors_bp)

    from app.views import blueprint as views_bp    
    app.register_blueprint(views_bp, url_prefix='')

    return app